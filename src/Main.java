import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Result: " + euclid(1785, 546));
        System.out.println("Prime Result: ");
        prime(5);

        int count = 0;
        int search = 10;
        int arr[] = {123,123,2,45};
        List<Integer> array= new ArrayList<>();
        List<Integer> arrayLinked= new LinkedList<>();

        int newArra[] = new int[arr.length + 1];
        array.add(123);

        array.add(123);
        array.get(3);
        System.out.println(arr[0]);

//        Random r = new Random();
//        r.nextInt(2000);
        for (int i : arr) {
            count++;
            if(i == search){
                System.out.println("Found it! " + i);
                System.out.println("It took me :" + count + " steps!");
            }
        }

        System.out.println();
        Arrays.sort(arr);

        binarySearch(arr,search);

    }

    public static void binarySearch(int[] arr, int x){
        int counter = 0;
        int f = 0;
        int l = arr.length - 1;
        int mid = (f+l)/2;


        while (f <= l) {
            counter++;
            if (arr[mid] == x) {
                System.out.println("Found it!("+ arr[mid] +") " + mid);
                System.out.println("It took me :" + counter + " steps!");
                return;
            }
            if (x > arr[mid]) {
                f = mid;
            }
            if (x < arr[mid]) {
                l = mid;
            }
            mid = (f + l) / 2;
        }

        System.out.println("Not found!");
    }


    public static void prime(int n){
        for(int i = 1; i <= n; i++){
            int counter = 0;
            for(int j = 1; j <= i; j++){
                if(i%j == 0){
                    counter++;
                }
            }
            if(counter == 2){
                System.out.println(i);
            }
        }
    }

    public static int euclid(int one, int two){
        int result = one%two;
        int previous = 0;
        while (result != 0){
            previous = result;
            one = two;
            two = result;
            result = one%two;
        }
        return previous;
    }
}
